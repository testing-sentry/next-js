import * as Sentry from '@sentry/node'


// export default (req, res) => {
//   try {
//     let obj = {};
    
//     obj.doSomething();
//     res.send('Success');
//   } catch (error) {
//     Sentry.captureException(error);
//     await Sentry.flush(2000);
//     res.status(500).send("Something oop");
//   }
// };

Sentry.init({
  dsn: process.env.dsn,
  release: process.env.release,
  debug: true,
});


export default async (req, res) => {
  try {
    let obj = {};
    
    obj.doSomething();
    res.send('Success');
  } catch (error) {
    Sentry.captureException(error);
    await Sentry.flush(2000);
    throw error;
    // res.status(500).send("Something oop");
  }
};