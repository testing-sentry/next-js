# Must have `sentry-cli` installed globally
# Following variable must be passed in
SENTRY_AUTH_TOKEN=$(SENTRY_ORG_TOKEN)

SENTRY_ORG=meredith
SENTRY_PROJECT=meow
VERSION=`sentry-cli releases propose-version`

setup_release: create_release associate_commits upload_sourcemaps

create_release:
	sentry-cli --log-level=debug releases --org $(SENTRY_ORG) new -p $(SENTRY_PROJECT) $(VERSION)

associate_commits:
	sentry-cli --log-level=debug releases --org $(SENTRY_ORG) -p $(SENTRY_PROJECT) set-commits --auto $(VERSION)

upload_sourcemaps:
	sentry-cli --log-level=debug releases --org $(SENTRY_ORG) -p $(SENTRY_PROJECT) files $(VERSION) upload-sourcemaps .next --rewrite --url-prefix '~/_next'